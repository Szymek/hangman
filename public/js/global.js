var player = null;
document.querySelector("#btnLogin").onclick = () => {
    let name = document.querySelector("#inputUsername").value;
    if (name.length < 3 || name.length > 15)
    {
        alert("Nick musi mieć co najmniej 3 i maksymalnie 15 znaków");
        return;
    }

    localStorage.username = name;

    player = new LPlayer(name);
    LocalGame.Player = player;
    player.openConnection()
        .then(() => {

            let nameBytes = Utils.EncodeString(name);
            let buffer, data;

            if (Config.RoomID == "") {
                buffer = new ArrayBuffer(1 + 1 + nameBytes.length); //id + name length + name (utf8)
                data = new DataView(buffer);
                data.setUint8(0, 1); //PacketID 1 // LOGIN
                data.setUint8(1, nameBytes.length);
                Utils.WriteStringToBuffer(data, nameBytes, 2);
            }
            else {
                let ridBytes = Utils.EncodeString(Config.RoomID);
                buffer = new ArrayBuffer(1 + 1 + 1 + nameBytes.length + ridBytes.length) //id + name length + room id length + name (utf8) + rid (utf8)
                data = new DataView(buffer);
                data.setUint8(0, 3); //PacketID 3 // JOIN_ROOM
                data.setUint8(1, ridBytes.length);
                Utils.WriteStringToBuffer(data, ridBytes, 2);
                data.setUint8(2 + ridBytes.length, nameBytes.length);
                Utils.WriteStringToBuffer(data, nameBytes, 3 + ridBytes.length);
            }
            player.sendPacket(buffer);
        });
};

document.querySelector("#btnSendSecret").onclick = () => {
    let input = document.querySelector("#inputSecret");
    if (!input.reportValidity())
        return;
    let word = input.value;

    let wordBytes = Utils.EncodeString(word);
    let buffer = new ArrayBuffer(1 + 1 + wordBytes.length); //id + word length + word (utf8)
    let data = new DataView(buffer);
    data.setUint8(0, 5); //PacketID 5 //SET_SECRET_WORD
    data.setUint8(1, wordBytes.length);
    Utils.WriteStringToBuffer(data, wordBytes, 2);
    LocalGame.Player.sendPacket(buffer);
    LocalGame.setWord(word);
};

(() => {
    let buttons = document.querySelectorAll(".playAgain");
    let redirectToHomepage = () => window.location = document.location.origin;
    for (let button of buttons)
        button.onclick = redirectToHomepage;
})();

if (localStorage.username != undefined)
    document.querySelector("#inputUsername").value = localStorage.username;


window.onload = () => LocalGame.addKeyboard();


document.querySelector("#btnRestartGame").onclick = () => {
    let buffer = new ArrayBuffer(1);
    let bytes = new Uint8Array(buffer);
    bytes[0] = 14; //PacketID 14 // PLAY_AGAIN_REQUEST
    LocalGame.Player.sendPacket(buffer);
};