var Sections = {
    switchTo: function(id)
    {
        let nodes = document.querySelectorAll(".game_section");
        for (let node of nodes) {
            if (!node.classList.contains("hidden"))
                node.classList.add("hidden");
        }
        document.querySelector("#" + id).classList.remove("hidden");
    },
    getCurrentSection: function() {
        return document.querySelector(".game_section:not(.hidden)").id;
    }
};