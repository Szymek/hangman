/**
 * Created by dszym on 23.10.2016.
 */

var LocalGame = {
    Player: null,
    EnemyName: null,
    EnemyWordLength: 0,
    Word: null,
    ID: null,
    IsLocalTour: false,
    setID: function(id) {
        LocalGame.ID = id;
        document.querySelector("#hInvURL").textContent = `${document.location.origin}/${id}`;
    },
    setEnemyName: function(name) {
        LocalGame.EnemyName = name;
        document.querySelector("#p2Name").textContent = name;
        console.log(`Enemy: ${name}`);
    },
    setWord: function(word) {
        word = word.toUpperCase();
        LocalGame.Word = word;
        let lettersContainer = document.querySelector("#localLetters");
        Utils.RemoveChildren(lettersContainer);
        for (let ch of word) {
            let letterElement = document.createElement("span");
            letterElement.textContent = ch;
            lettersContainer.appendChild(letterElement)
        }
    },
    setEnemyWordLength: function(len)
    {
        let lettersContainer = document.querySelector("#enemyLetters");
        Utils.RemoveChildren(lettersContainer);
        for (let i = 0; i < len; i++)
        {
            let emptySpace = document.createElement("h1");
            emptySpace.textContent = "_";
            lettersContainer.appendChild(emptySpace);
        }
        LocalGame.EnemyWordLength = len;
    },
    setLocalTour: function(isMine)
    {
        LocalGame.IsLocalTour = isMine;

        let tourOwnerElement = document.querySelector("#tourOwner");
        if (isMine)
        {
            tourOwnerElement.classList.remove("enemy_tour");
            if (!tourOwnerElement.classList.contains("my_tour"))
                tourOwnerElement.classList.add("my_tour");
            tourOwnerElement.textContent = "Twoja kolej";
        }
        else
        {
            tourOwnerElement.classList.remove("my_tour");
            if (!tourOwnerElement.classList.contains("enemy_tour"))
                tourOwnerElement.classList.add("enemy_tour");
            tourOwnerElement.textContent = `Tura gracza ${LocalGame.EnemyName}`;
        }
    },
    addKeyboard: function() {
        let keys = ['A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'O', 'Ó', 'P', 'Q', 'R', 'S', 'Ś', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Ż', 'Ź'];
        let keysContainer = document.querySelector(".keys");
        Utils.RemoveChildren(keysContainer);

        let sendKey = (e) => {
            if (!LocalGame.IsLocalTour)
                return;

            let element = e.target;

            let buffer = new ArrayBuffer(3);
            let data = new DataView(buffer);
            data.setUint8(0, 8); //PacketID 8 // USE_THE_LETTER
            data.setUint16(1, element.innerText.charCodeAt(0));
            LocalGame.Player.sendPacket(buffer);

            element.classList.add("used");
            element.removeEventListener("click", sendKey); //in case of unspoported browser
        };
        for (let key of keys)
        {
            let keyElement = document.createElement("div");
            keyElement.textContent = key;
            keyElement.addEventListener("click", sendKey, {once: true}); //chrome 55+, firefox 50+
            keysContainer.appendChild(keyElement);
        }
    }
};