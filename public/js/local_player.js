class LPlayer {
    constructor(name)
    {
        this.Name = name;
        this.Socket = null;

        document.querySelector("#p1Name").textContent = name;
    }
    openConnection() {
        if (this.Socket != null)
            throw new Error("Socket is already defined");
        this.Socket = new WebSocket(Config.SocketServer);
        this.Socket.binaryType = "arraybuffer";
        this.Socket.onmessage = (p) => this.handlePacket(p);
        this.Socket.onclose = () => {
            if (Sections.getCurrentSection() == "section_fatal_error")
                return;

            document.querySelector("#section_fatal_error .errorMessage").textContent = "Utracono połączenie z serwerem";
            Sections.switchTo("section_fatal_error");
        };
        return new Promise((resolve, reject) => {
            this.Socket.onopen = resolve;
        });
    }
    handlePacket(packet)
    {
        let buffer = packet.data;
        let data = new DataView(buffer);

        let pid = data.getUint8(0);
        console.log(`Packet received (${pid})`);

        switch (pid)
        {
            case 2:
                this._handleRoomIDPacket(data);
                break;
            case 4:
                this._handleSecondPlayerPacket(data);
                break;
            case 6: //SECRET_NOT_VALID
                alert("Wprowadzone słowo nie jest prawidłowe");
                break;
            case 7:
                this._handleSecretWordSavedPacket(data);
                break;
            case 9:
                this._handleRevealOwnLetterPacket(data);
                break;
            case 10:
                this._handleRevealEnemyLetterPacket(data);
                break;
            case 11:
                this._handleChangeTourPacket(data);
                break;
            case 12:
                this._handleGameOverPacket(data);
                break;
            case 13:
                this._handleEnemyDisconnectedPacket(data);
                break;
            case 15:
                this._handleCleanUpPacket(data);
                break;
            default:
                if (pid > 100) {
                    this._handleError(pid);
                    break;
                }
                console.log("Unknown packet");
                break;
        }
    }
    sendPacket(data)
    {
        this.Socket.send(data);
        console.log(`${data.byteLength} bytes sent`);
    }

    _handleRoomIDPacket(data)
    {
        let rid = Utils.ReadStringFromBuffer(data, 2);
        Config.RoomID = rid;

        console.log("RID: " + rid);

        LocalGame.setID(rid);
        Sections.switchTo("section_send_inv");
    }

    _handleSecondPlayerPacket(data)
    {
        let name = Utils.ReadStringFromBuffer(data, 2);
        LocalGame.setEnemyName(name);
        Sections.switchTo("section_enter_secret");
    }

    _handleSecretWordSavedPacket(data)
    {
        LocalGame.setLocalTour(data.getUint8(3));

        if (data.getUint8(1) == 0)
            Sections.switchTo("section_waiting_for_player");
        else {
            LocalGame.setEnemyWordLength(data.getUint8(2));
            Sections.switchTo("section_the_game");
        }
    }
    _handleChangeTourPacket(data)
    {
        LocalGame.setLocalTour(data.getUint8(1));
    }
    _handleRevealOwnLetterPacket(data)
    {
        document.querySelector(`#localLetters *:nth-child(${data.getUint8(1) + 1})`).classList.add("hit");
    }
    _handleRevealEnemyLetterPacket(data)
    {
        let element = document.querySelector(`#enemyLetters *:nth-child(${data.getUint8(1) + 1})`);
        element.classList.add("hit");
        element.textContent = String.fromCharCode(data.getUint16(2));
    }
    _handleGameOverPacket(data) {
        let won = data.getUint8(1);
        let winnerElement = document.querySelector("#theWinner");
        var elClass = "";
        if (won) {
            winnerElement.textContent = `Gratulacje! Pokonałeś ${LocalGame.EnemyName}`;
            elClass = "localPlayer";
        }
        else {
            winnerElement.textContent = `${LocalGame.EnemyName} został zwycięzcą!`;
            elClass = "enemyPlayer";
        }

        winnerElement.classList.add(elClass);
        document.querySelector(".finalInfo").style.display = "initial";
        console.log(`Won: ${won}`);
    }
    _handleEnemyDisconnectedPacket(data) {
        document.querySelector("#section_fatal_error .errorMessage").textContent = "Przeciwnik się rozłączył";
        Sections.switchTo("section_fatal_error");
    }
    _handleCleanUpPacket(data) {
        LocalGame.addKeyboard();
        document.querySelector("#inputSecret").value = "";
        document.querySelector("#theWinner").setAttribute("class", "");
        document.querySelector(".finalInfo").setAttribute("style", "");
    }

    _handleError(id)
    {
        if (id == 111)
        {
            alert("Podany pokój nie istnieje");
            window.location = document.location.origin;
        }
        else
            alert("Wystąpił nieznany błąd");
    }
}