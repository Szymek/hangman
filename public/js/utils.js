var TextDecoder = new TextDecoder();
var TextEncoder = new TextEncoder();
var Utils = {
    WriteStringToBuffer(dataview, str, offset)
    {
        offset = offset || 0;
        var arr = typeof(str) == "string" || str instanceof String ? new TextEncoder().encode(str) : str;
        for (let char of arr)
            dataview.setUint8(offset++, char);
    },
    ReadStringFromBuffer(buffer, offset)
    {
        let stringLength = buffer.getUint8(offset - 1);
        let bytes = new Uint8Array(stringLength);
        for (let i = 0; i < stringLength; i++, offset++)
            bytes[i] = buffer.getUint8(offset);
        return TextDecoder.decode(bytes);
    },
    EncodeString(str)
    {
        return TextEncoder.encode(str);
    },
    DecodeString(bytes) {
        return TextDecoder.decode(bytes);
    },
    RemoveChildren(node) {
        while (node.lastChild)
            node.removeChild(node.lastChild);
    }
};