/**
 * Created by dszym on 22.10.2016.
 */

var Utils = require("./Utils");

class Player {
    constructor(username, socket) {
        //this.Name = Utils.EncodeHTML(username); //BUG: textContent displays html entities
        this.Name = username;
        this.Socket = socket;
        this.SecretWord = this._secretWord = null;
        this.WordObject = [];
    }

    SendPacket(data)
    {
        this.Socket.send(data);
        console.log(`${data.byteLength} bytes sent`);
    }

    set SecretWord(word)
    {
        if (typeof(word) != "string" && !(word instanceof String))
        {
            this._secretWord = word;
            return;
        }

        word = word.toUpperCase();
        //this._secretWord = Utils.EncodeHTML(word); //BUG: textContent displays html entities
        this._secretWord = word;

        this.WordObject = [];
        for (let ch of word)
            this.WordObject.push({Letter: ch, Hit: false});
    }
    get SecretWord() {
        return this._secretWord;
    }

}

module.exports = Player;