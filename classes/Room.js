/**
 * Created by dszym on 22.10.2016.
 */
var Player = require("../classes/Player");
var Utils = require("../classes/Utils");
var Packets = require("../enums/Packets");
var RoomState = require("../enums/RoomState");

class Room {
    constructor(id) {
        this.ID = id;
        this.Player1 = null;
        this.Player2 = null;
        this.State = RoomState.WAITING_FOR_SECOND_PLAYER;
        this.PlayerTour = 1;
    }
    HandlePacket(socket, id, buffer)
    {
        switch (id)
        {
            case Packets.JOIN_ROOM:
                this._handleJoinRoomPacket(socket, buffer);
                break;
            case Packets.SET_SECRET_WORD:
                this._handleSetSecretPacket(socket, buffer);
                break;
            case Packets.USE_THE_LETTER:
                this._handleUseLetterPacket(socket, buffer);
                break;
            case Packets.PLAY_AGAIN_REQUEST:
                this._handlePlayAgainRequestPacket(socket, buffer);
                break;
            default:
                console.log(`Unknown packet (${id})`);
        }
    }
    _handleJoinRoomPacket(socket, buffer)
    {
        if (this.Player2 != null)
            socket.disconnect();

        let name = Utils.ReadStringFromBuffer(buffer, 2 + this.ID.length + 1);
        this.Player2 = new Player(name, socket);


        this.Player1.SendPacket(this.__craftJoinPacket(name));
        this.Player2.SendPacket(this.__craftJoinPacket(this.Player1.Name));
        this.State = RoomState.ENTERING_SECRETS;
    }
    _handleSetSecretPacket(socket, buffer)
    {
        if (this.State != RoomState.ENTERING_SECRETS && this.State != RoomState.WAITING_FOR_REPLAY)
            return;

        let player = this.EstimatePlayerBySocket(socket);
        let word = Utils.ReadStringFromBuffer(buffer, 2).toLowerCase();

        if (!(/^[a-zęóąśłżźćń]{1,15}$/).test(word))
        {
            let buffer = new ArrayBuffer(1);
            let arr = new Uint8Array(buffer);
            arr[0] = Packets.SECRET_NOT_VALID;
            player.SendPacket(buffer);
            return;
        }

        player.SecretWord = word;
        let secondPlayer = this.GetSecondPlayer(player);
        buffer = new ArrayBuffer(4);
        let bytes = new Uint8Array(buffer);
        bytes[0] = Packets.SECRET_WORD_SAVED;
        if (this.State == RoomState.ENTERING_SECRETS && secondPlayer.SecretWord != null)
        {
            bytes[1] = 1; //go into the game
            bytes[2] = secondPlayer.SecretWord.length;

            let spBuffer = new ArrayBuffer(4);
            let spBytes = new Uint8Array(spBuffer);
            spBytes[0] = Packets.SECRET_WORD_SAVED;
            spBytes[1] = 1;
            spBytes[2] = word.length;
            spBytes[3] = this.IsTourOwner(secondPlayer);
            secondPlayer.SendPacket(spBuffer);

            this.State = RoomState.THE_GAME;
        }
        else
            bytes[1] = 0; //wait screen

        bytes[3] = this.IsTourOwner(player);

        player.SendPacket(buffer);
    }
    _handleUseLetterPacket(socket, buffer)
    {
        if (this.State != RoomState.THE_GAME)
        {
            console.log(`Room (${this.ID}) state ${this.State} is not RoomState.THE_GAME`);
            return;
        }

        let sender = this.EstimatePlayerBySocket(socket);
        if (!this.IsTourOwner(sender)) {
            console.log("Tour abuse");
            return;
        }

        let victim = this.GetSecondPlayer(sender);
        let charCode = buffer.readUInt16BE(1);
        let char = String.fromCharCode(charCode);
        let letters = victim.WordObject;
        var hit = -1;

        for (let i = 0; i < letters.length; i++)
        {
            if (letters[i].Letter == char)
            {
                let buffer = new ArrayBuffer(4);
                let data = new DataView(buffer);
                data.setUint8(0, Packets.REVEAL_ENEMY_LETTER);
                data.setUint8(1, i);
                data.setUint16(2, charCode);
                sender.SendPacket(buffer);


                buffer = new ArrayBuffer(2);
                let bytes = new Uint8Array(buffer);
                bytes[0] = Packets.REVEAL_OWN_LETTER;
                bytes[1] = i;
                victim.SendPacket(buffer);

                letters[i].Hit = true;
                hit = i;

                //do not break in case of same letters
            }
        }

        if (hit != -1) {
            var won = true;
            for (let letter of letters)
            {
                if (!letter.Hit)
                {
                    won = false;
                    break;
                }
            }

            if (!won)
                return;

            this.State = RoomState.GAME_OVER;
            let craftEnemyLetterPacket = (pos, code) => {
                let buffer = new ArrayBuffer(4);
                let data = new DataView(buffer);
                data.setUint8(0, Packets.REVEAL_ENEMY_LETTER);
                data.setUint8(1, pos);
                data.setUint16(2, code);
                return buffer;
            };
            for (let i = 0; i < sender.WordObject.length; i++)
            {
                let letter = sender.WordObject[i];
                if (!letter.Hit)
                    victim.SendPacket(craftEnemyLetterPacket(i, letter.Letter.charCodeAt(0)));
            }


            let craftGameOverPacket = (won) => {
                let buffer = new ArrayBuffer(2);
                let bytes = new Uint8Array(buffer);
                bytes[0] = Packets.GAME_OVER;
                bytes[1] = won;
                return buffer;
            };
            sender.SendPacket(craftGameOverPacket(true));
            victim.SendPacket(craftGameOverPacket(false));
        }
        else {
            this.SwitchTour();
            let buffer = new ArrayBuffer(2);
            let bytes = new Uint8Array(buffer);
            bytes[0] = Packets.CHANGE_TOUR;
            bytes[1] = this.IsTourOwner(this.Player1);
            this.Player1.SendPacket(buffer);
            bytes[1] = this.IsTourOwner(this.Player2);
            this.Player2.SendPacket(buffer);
        }
    }

    _handlePlayAgainRequestPacket(socket, buffer)
    {
        if (this.State != RoomState.WAITING_FOR_REPLAY && this.State != RoomState.GAME_OVER)
            return;

        let player = this.EstimatePlayerBySocket(socket);
        let enemy = this.GetSecondPlayer(player);
        buffer = new ArrayBuffer(1);
        let bytes = new Uint8Array(buffer);
        bytes[0] = Packets.CLEAN_UP;
        player.SendPacket(buffer);
        player.SendPacket(this.__craftJoinPacket(enemy.Name));

        if (this.State == RoomState.GAME_OVER) {
            player.SecretWord = null;
            enemy.SecretWord = null;
            this.SwitchTour();
            this.State = RoomState.WAITING_FOR_REPLAY;
        }
        else
            this.State = RoomState.ENTERING_SECRETS;
    }

    EstimatePlayerBySocket(socket)
    {
        if (this.Player1 != null &&  this.Player1.Socket == socket)
            return this.Player1;
        return this.Player2;
    }

    GetSecondPlayer(player)
    {
        if (this.Player1 == player)
            return this.Player2;

        return this.Player1;
    }

    IsTourOwner(player)
    {
        return (this.PlayerTour == 1 && this.Player1 == player) || (this.PlayerTour == 2 && this.Player2 == player);
    }
    SwitchTour() {
        this.PlayerTour = this.PlayerTour == 1 ? 2 : 1;
    }


    __craftJoinPacket(name)
    {
        let nameBytes = Utils.EncodeString(name);
        let buffer = new ArrayBuffer(1 + 1 + nameBytes.length);
        let data = new DataView(buffer);
        data.setUint8(0, Packets.SECOND_PLAYER_CONNECTED);
        data.setUint8(1, nameBytes.length);
        Utils.WriteStringToBuffer(data, nameBytes, 2);
        return buffer;
    };
}

module.exports = Room;