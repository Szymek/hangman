/**
 * Created by dszym on 22.10.2016.
 */

var _textEncoding = require("text-encoding");
var TextDecoder = new _textEncoding.TextDecoder();
var TextEncoder = new _textEncoding.TextEncoder();
var htmlencode = require("htmlencode").htmlEncode;

var Utils = {
    WriteStringToBuffer(dataview, str, offset)
    {
        offset = offset || 0;
        var arr = typeof(str) == "string" || str instanceof String ? new TextEncoder().encode(str) : str;
        for (let char of arr)
            dataview.setUint8(offset++, char);
    },
    ReadStringFromBuffer(buffer, offset)
    {
        let stringLength = buffer.readUInt8(offset - 1);
        let bytes = new Uint8Array(stringLength);
        for (let i = 0; i < stringLength; i++, offset++)
            bytes[i] = buffer.readUInt8(offset);
       return TextDecoder.decode(bytes);
    },
    EncodeString(str)
    {
        return TextEncoder.encode(str);
    },
    EncodeHTML(str)
    {
        return htmlencode(str);
    }
};

module.exports = Utils;