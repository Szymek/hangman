var express = require('express');
var path = require('path');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.enable("view cache");

app.use("/public", express.static(path.join(__dirname, 'public')));
app.get("/favicon.ico", (req, res) => res.end());




//globals
global.Config = require("./config.json");

//routes
app.use('/', require("./routes/index"));



app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next();
});



if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
else {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message
    });
  });
}


require("./modules/game_server");
module.exports = app;
