/**
 * Created by dszym on 22.10.2016.
 */

var Player = require("../classes/Player");
var Room = require("../classes/Room");
var ShortID = require("shortid");
var _textEncoding = require("text-encoding");
var TextDecoder = new _textEncoding.TextDecoder();
var TextEncoder = new _textEncoding.TextEncoder();
var Packets = require("../enums/Packets");
var Utils = require("../classes/Utils");


var Game = {
    Rooms: {},
    New: function(id) {
        id = id || ShortID.generate().toLowerCase();
        return Game.Rooms[id] = new Room(id);
    },
    Remove: function(id) {
        var room = Game.Rooms[id];
        if (room == undefined)
            return;
        if (room.Player1 != null && room.Player1.Socket != null)
            room.Player1.Socket.close();
        if (room.Player2 != null && room.Player2.Socket != null)
            room.Player2.Socket.close();
        delete Game.Rooms[id];
    },
    FindRoomBySocket: function(socket) {
        for (let [id, room] of Object.entries(Game.Rooms))
        {
            if ((room.Player1 != null && room.Player1.Socket == socket) ||
                (room.Player2 != null && room.Player2.Socket == socket))
                return room;
        }
        return null;
    },
    HandleNewRoom: function(socket, buffer) {
        let name = Utils.ReadStringFromBuffer(buffer, 2);
        let room = Game.New();

        room.Player1 = new Player(name);
        room.Player1.Socket = socket;


        let ridBytes = Utils.EncodeString(room.ID);
        buffer = new ArrayBuffer(1 + 1 + ridBytes.length);
        let data = new DataView(buffer);
        data.setUint8(0, Packets.ROOM_ID);
        data.setUint8(1, ridBytes.length);
        Utils.WriteStringToBuffer(data, ridBytes, 2);
        room.Player1.SendPacket(buffer);
    }
};

module.exports = Game;