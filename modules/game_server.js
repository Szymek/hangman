/**
 * Created by dszym on 22.10.2016.
 */

var server = new require("ws").Server({port: Config.socket_server_port});
var Packets = require("../enums/Packets");
var Game = require("../modules/game");
var Utils = require("../classes/Utils")
var RoomState = require("../enums/RoomState");

server.on("connection", socket => {

    socket.on("message", packet => {
        if (!packet instanceof Uint8Array || packet.length == 0)
            return;


        let buffer = new Buffer(packet);
        let pid = buffer.readUInt8(0);
        console.log(`Packet received (${pid})`);

        let room = Game.FindRoomBySocket(socket);
        if (room == null)
        {
            if (pid == Packets.LOGIN) {
                Game.HandleNewRoom(socket, buffer);
                return;
            }
            else if (pid == Packets.JOIN_ROOM)
            {
                let rid = Utils.ReadStringFromBuffer(buffer, 2);
                room = Game.Rooms[rid];
                if (room != undefined) {
                    room.HandlePacket(socket, pid, buffer);
                    return;
                }
            }


            let ab = new ArrayBuffer(1);
            let data = new DataView(ab);
            data.setUint8(0, Packets.ERR_ROOM_DOESNT_EXIST);
            socket.send(ab);

            return;
        }

        room.HandlePacket(socket, pid, buffer);
    });

    socket.on("close", () => {
        let room = Game.FindRoomBySocket(socket);
        if (room != null)
        {
            if (room.State != RoomState.WAITING_FOR_SECOND_PLAYER && room.Player2 != null) {
                let disconnectedPlayer = room.EstimatePlayerBySocket(socket);
                let enemyPlayer = room.GetSecondPlayer(disconnectedPlayer);

                let buffer = new ArrayBuffer(1);
                let bytes = new Uint8Array(buffer);
                bytes[0] = Packets.PLAYER_DISCONNECTED;
                enemyPlayer.SendPacket(buffer);
            }

            Game.Remove(room.ID);
        }
    });
});

module.exports = server;