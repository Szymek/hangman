var express = require('express');
var router = express.Router();


router.get('/:roomID?', function(req, res, next) {
    let rid = req.params.roomID || "";
    res.render("main_page", {roomID: rid});
});

module.exports = router;
