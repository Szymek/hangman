/**
 * Created by dszym on 22.10.2016.
 */

var Packets = {
    LOGIN: 1, //C -> S
    ROOM_ID: 2, //S -> C,
    JOIN_ROOM: 3, //C -> S
    SECOND_PLAYER_CONNECTED: 4, //S -> C
    SET_SECRET_WORD: 5, //C -> S
    SECRET_NOT_VALID: 6, //S -> C
    SECRET_WORD_SAVED: 7, //S -> C
    USE_THE_LETTER: 8, //C -> S
    REVEAL_OWN_LETTER: 9, //S -> C
    REVEAL_ENEMY_LETTER: 10, //S -> C
    CHANGE_TOUR: 11, //S -> C
    GAME_OVER: 12, //S -> C
    PLAYER_DISCONNECTED: 13, //S -> C
    PLAY_AGAIN_REQUEST: 14, //C -> S
    CLEAN_UP: 15, //S -> C

    ERR_ROOM_DOESNT_EXIST: 111 //S -> C
};

module.exports = Packets;