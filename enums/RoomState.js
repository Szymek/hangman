/**
 * Created by dszym on 23.10.2016.
 */

var RoomState = {
    WAITING_FOR_SECOND_PLAYER: 0,
    ENTERING_SECRETS: 1,
    THE_GAME: 2,
    GAME_OVER: 3,
    WAITING_FOR_REPLAY: 4
};

module.exports = RoomState;