## List of packets
First byte of a packet is its ID.
String are preceded with unsigned byte containing their length.
Errors IDs starts from 100.

| Packet ID | Packet name             | Send to | Fields                                                                                                |
|-----------|-------------------------|---------|-------------------------------------------------------------------------------------------------------|
| 1         | LOGIN                   | Server  | username (String)                                                                                     |
| 2         | ROOM_ID                 | Client  | room_id (String)                                                                                      |
| 3         | JOIN_ROOM               | Server  | room_id (String)<br>username (String)                                                                   |
| 4         | SECOND_PLAYER_CONNECTED | Client  | username of 2nd player (String)                                                                       |
| 5         | SET_SECRET_WORD         | Server  | word (String)                                                                                         |
| 6         | SECRET_NOT_VALID        | Client  |                                                                                                       |
| 7         | SECRET_WORD_SAVED       | Client  | is second player ready (Boolean)<br>second player's word length (ubyte)<br>is player a tour owner (Boolean) |
| 8         | USE_THE_LETTER          | Server  | unicode of character (ushort)                                                                         |
| 9         | REVEAL_OWN_LETTER       | Client  | letter's position counting from 0 (ubyte)                                                             |
| 10        | REVEAL_ENEMY_LETTER     | Client  | letter's position counting from 0 (ubyte)<br>unicode of character (ushort)                               |
| 11        | CHANGE_TOUR             | Client  | is player a tour owner (Boolean)                                                                      |
| 12        | GAME_OVER               | Client  | is player a winner (Boolean)                                                                          |
| 13        | ENEMY_DISCONNECTED      | Client  |                                                                                                       |
| 14        | PLAY_AGAIN_REQUEST      | Server  |                                                                                                       |
| 15        | CLEAN_UP                | Client  |                                                                                                       |
|           |                         |         |                                                                                                       |
|           |                         |         |                                                                                                       |
|           |                         |         |                                                                                                       |
|           |                         |         |                                                                                                       |
| 111       | ERR_ROOM_DOESNT_EXIST   | Client  |                                                                                                       |
